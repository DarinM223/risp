#![feature(box_patterns)]

mod builtin;
mod data;
mod environment;
mod interpreter;
mod lexer;
mod parser;

fn main() {
    println!("Hello world!");
}
