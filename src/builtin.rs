use data::Atom;
use interpreter::{EvalError, Result};

pub fn builtin_car(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(box Atom::Nil, box Atom::Nil) => Ok(Atom::Nil),
        Atom::Pair(box Atom::Pair(car, _), box Atom::Nil) => Ok(*car),
        Atom::Pair(_, box Atom::Nil) => Err(EvalError::DifferentType),
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_cdr(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(box Atom::Nil, box Atom::Nil) => Ok(Atom::Nil),
        Atom::Pair(box Atom::Pair(_, cdr), box Atom::Nil) => Ok(*cdr),
        Atom::Pair(_, box Atom::Nil) => Err(EvalError::DifferentType),
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_cons(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => Ok(Atom::Pair(car, cdr)),
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_add(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => match (*car, *cdr) {
            (Atom::Integer(a), Atom::Integer(b)) => Ok(Atom::int(a + b)),
            _ => Err(EvalError::DifferentType),
        },
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_sub(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => match (*car, *cdr) {
            (Atom::Integer(a), Atom::Integer(b)) => Ok(Atom::int(a - b)),
            _ => Err(EvalError::DifferentType),
        },
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_mul(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => match (*car, *cdr) {
            (Atom::Integer(a), Atom::Integer(b)) => Ok(Atom::int(a * b)),
            _ => Err(EvalError::DifferentType),
        },
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_div(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => match (*car, *cdr) {
            (Atom::Integer(a), Atom::Integer(b)) => Ok(Atom::int(a / b)),
            _ => Err(EvalError::DifferentType),
        },
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_numeq(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => match (*car, *cdr) {
            (Atom::Integer(a), Atom::Integer(b)) if a == b => Ok(Atom::sym("T")),
            (Atom::Integer(_), Atom::Integer(_)) => Ok(Atom::Nil),
            _ => Err(EvalError::DifferentType),
        },
        _ => Err(EvalError::InvalidArgs),
    }
}

pub fn builtin_less(args: Atom) -> Result<Atom> {
    match args {
        Atom::Pair(car, box Atom::Pair(cdr, box Atom::Nil)) => match (*car, *cdr) {
            (Atom::Integer(a), Atom::Integer(b)) if a < b => Ok(Atom::sym("T")),
            (Atom::Integer(_), Atom::Integer(_)) => Ok(Atom::Nil),
            _ => Err(EvalError::DifferentType),
        },
        _ => Err(EvalError::InvalidArgs),
    }
}
