use data::Atom;
use lexer;
use std::borrow::Cow;
use std::result;

#[derive(Clone, Debug, PartialEq)]
pub enum ParserError {
    Eof,
    UnexpectedEndParen,
    ExpectedEndParen,
    UnexpectedDot,
}

pub type Result<T> = result::Result<T, ParserError>;

pub fn read_simple(tokens: &mut impl Iterator<Item = String>) -> Result<Atom> {
    tokens
        .next()
        .ok_or(ParserError::Eof)
        .and_then(|token| parse_simple(token))
}

pub fn read_list(tokens: &mut impl Iterator<Item = String>) -> Result<Atom> {
    let mut stack = Vec::new();

    loop {
        let token = tokens.next().ok_or(ParserError::Eof)?;
        match token.as_ref() {
            ")" => return Ok(stack_to_atom(&mut stack, false)),
            "." => {
                if stack.is_empty() {
                    return Err(ParserError::UnexpectedDot);
                }

                stack.push(read_expr(tokens)?);
                if tokens.next() != Some(")".to_string()) {
                    return Err(ParserError::ExpectedEndParen);
                }
                return Ok(stack_to_atom(&mut stack, true));
            }
            token => {
                stack.push(parse_expr(tokens, token.to_string())?);
            }
        }
    }
}

pub fn read_expr(tokens: &mut impl Iterator<Item = String>) -> Result<Atom> {
    tokens
        .next()
        .ok_or(ParserError::Eof)
        .and_then(|token| parse_expr(tokens, token))
}

fn parse_simple(token: String) -> Result<Atom> {
    if let Ok(i) = token.parse::<i32>() {
        return Ok(Atom::int(i));
    }

    let token = token.to_uppercase();
    if token == "NIL".to_string() {
        Ok(Atom::Nil)
    } else {
        Ok(Atom::sym(token))
    }
}

fn parse_expr(tokens: &mut impl Iterator<Item = String>, token: String) -> Result<Atom> {
    match token.as_ref() {
        "(" => read_list(tokens),
        ")" => Err(ParserError::UnexpectedEndParen),
        token => parse_simple(token.to_string()),
    }
}

fn stack_to_atom(s: &mut Vec<Atom>, dot: bool) -> Atom {
    if dot && s.last().map(|s| *s == Atom::Nil).unwrap_or(false) {
        s.pop();
    }
    stack_to_atom_rec(s, Atom::Nil)
}

fn stack_to_atom_rec(s: &mut Vec<Atom>, build: Atom) -> Atom {
    match s.pop() {
        Some(last) => stack_to_atom_rec(s, Atom::cons(last, build)),
        _ => build,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_simple_int() {
        let mut tokens = lexer::lex("123");
        assert_eq!(read_simple(&mut tokens), Ok(Atom::Integer(123)));
    }

    #[test]
    fn test_parse_simple_symbol() {
        let mut tokens = lexer::lex("hello");
        assert_eq!(
            read_simple(&mut tokens),
            Ok(Atom::Symbol("HELLO".to_string()))
        );
    }

    #[test]
    fn test_parse_simpl_nil() {
        let mut tokens = lexer::lex("nil");
        assert_eq!(read_simple(&mut tokens), Ok(Atom::Nil));
    }

    #[test]
    fn test_parse_list() {
        let mut tokens = lexer::lex("1 2 3)");
        assert_eq!(
            read_list(&mut tokens),
            Ok(Atom::cons(
                Atom::int(1),
                Atom::cons(Atom::int(2), Atom::cons(Atom::int(3), Atom::Nil))
            ))
        );
    }

    #[test]
    fn test_dot_list() {
        let mut tokens = lexer::lex("1 2 . 3)");
        assert_eq!(
            read_list(&mut tokens),
            Ok(Atom::cons(
                Atom::int(1),
                Atom::cons(Atom::int(2), Atom::cons(Atom::int(3), Atom::Nil))
            ))
        );
    }

    #[test]
    fn test_list_expl_nil() {
        let mut tokens = lexer::lex("1 2 3 . nil)");
        assert_eq!(
            read_list(&mut tokens),
            Ok(Atom::cons(
                Atom::int(1),
                Atom::cons(Atom::int(2), Atom::cons(Atom::int(3), Atom::Nil))
            ))
        );
    }

    #[test]
    fn test_empty_list_nil() {
        let mut tokens = lexer::lex("()");
        assert_eq!(read_expr(&mut tokens), Ok(Atom::Nil));
    }

    #[test]
    fn test_if() {
        let mut tokens = lexer::lex("(if nil 3 4)");
        assert_eq!(
            read_expr(&mut tokens),
            Ok(Atom::cons(
                Atom::sym("IF"),
                Atom::cons(
                    Atom::Nil,
                    Atom::cons(Atom::int(3), Atom::cons(Atom::int(4), Atom::Nil))
                )
            ))
        );
    }
}
