use data::Atom;
use std::mem;

pub trait Environment<K, V> {
    fn get<'a>(&'a self, sym: &K) -> Option<&'a V>;
    fn set(&mut self, sym: K, value: V);
}

impl Atom {
    pub fn new_env(parent: Atom) -> Atom {
        Atom::cons(parent, Atom::Nil)
    }
}

impl Environment<Atom, Atom> for Atom {
    fn get<'a>(&'a self, sym: &Atom) -> Option<&'a Atom> {
        if let Atom::Pair(ref car, ref cdr) = *self {
            let mut bs = cdr;
            while let Atom::Pair(box Atom::Pair(ref b_car, ref b_cdr), ref cdr) = **bs {
                if **b_car == *sym {
                    return Some(b_cdr);
                }
                bs = cdr;
            }

            if **car == Atom::Nil {
                return None;
            }
            return car.get(sym);
        }

        None
    }

    fn set(&mut self, sym: Atom, value: Atom) {
        if let Atom::Pair(_, ref mut cdr) = *self {
            // Mutate binding internally if it already exists.
            unsafe {
                let mut bs = cdr as *mut Box<Atom>;
                while let Atom::Pair(box Atom::Pair(ref b_car, ref mut b_cdr), ref mut cdr) = **bs {
                    if **b_car == sym {
                        **b_cdr = value;
                        return;
                    }
                    bs = cdr as *mut Box<Atom>;
                }
            }

            let temp_cdr = mem::replace(cdr, Box::new(Atom::Nil));
            *cdr = Box::new(Atom::cons(Atom::cons(sym, value), *temp_cdr));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_set() {
        let mut env = Atom::new_env(Atom::Nil);
        assert_eq!(env.get(&Atom::sym("age")), None);
        env.set(Atom::sym("age"), Atom::int(12));
        assert_eq!(*env.get(&Atom::sym("age")).unwrap(), Atom::int(12));
        env.set(Atom::sym("age"), Atom::int(21));
        assert_eq!(*env.get(&Atom::sym("age")).unwrap(), Atom::int(21));
    }
}
