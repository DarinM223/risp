use data::Atom;
use environment::Environment;
use std::result;

macro_rules! clone_opt_ref {
    ($opt:expr) => {
        match $opt {
            opt @ Some(..) => opt.cloned(),
            None => None,
        }
    };
}

#[derive(Clone, Debug, PartialEq)]
pub enum EvalError {
    InvalidSyntax,
    NoBinding(Atom),
    InvalidArgs,
    DifferentType,
}

pub type Result<T> = result::Result<T, EvalError>;

impl Atom {
    pub fn apply(&self, args: Atom) -> Result<Atom> {
        match *self {
            Atom::Builtin(ref b) => (*b.fun)(args),
            Atom::Closure(box Atom::Pair(ref env, box Atom::Pair(ref arg_names, ref body))) => {
                // FIXME(DarinM223): Prevent cloning the environment twice when creating lambdas.
                let mut env = Atom::new_env(*env.clone());
                let mut args = &Box::new(args);
                let mut arg_names = arg_names;
                while let Atom::Pair(ref car_names, ref cdr_names) = **arg_names {
                    match **args {
                        Atom::Nil => return Err(EvalError::InvalidArgs),
                        Atom::Pair(ref car_args, ref cdr_args) => {
                            env.set(*car_names.clone(), *car_args.clone());
                            arg_names = cdr_names;
                            args = cdr_args;
                        }
                        _ => return Err(EvalError::InvalidSyntax),
                    }
                }
                if **args != Atom::Nil {
                    return Err(EvalError::InvalidArgs);
                }

                let mut body = body;
                let mut result = Atom::Nil;
                while let Atom::Pair(ref car, ref cdr) = **body {
                    result = car.eval(&mut env)?;
                    body = cdr;
                }

                Ok(result)
            }
            _ => Err(EvalError::DifferentType),
        }
    }

    pub fn eval(&self, env: &mut Atom) -> Result<Atom> {
        match *self {
            ref s @ Atom::Symbol(..) => {
                clone_opt_ref!(env.get(&s)).ok_or(EvalError::NoBinding(s.clone()))
            }
            ref p @ Atom::Pair(..) => {
                if !p.is_list() {
                    return Err(EvalError::InvalidSyntax);
                }

                match *p {
                    Atom::Pair(box Atom::Symbol(ref s), ref args) if s.as_str() == "QUOTE" => {
                        if let Atom::Pair(ref val, box Atom::Nil) = **args {
                            Ok(*val.clone())
                        } else {
                            Err(EvalError::InvalidArgs)
                        }
                    }
                    Atom::Pair(box Atom::Symbol(ref s), ref args) if s.as_str() == "DEFINE" => {
                        if let Atom::Pair(ref sym, box Atom::Pair(ref expr, box Atom::Nil)) = **args
                        {
                            match **sym {
                                Atom::Symbol(..) => {}
                                _ => return Err(EvalError::DifferentType),
                            }

                            let val = expr.eval(env)?;
                            env.set(*sym.clone(), val);

                            Ok(*sym.clone())
                        } else {
                            Err(EvalError::InvalidArgs)
                        }
                    }
                    Atom::Pair(box Atom::Symbol(ref s), ref args) if s.as_str() == "LAMBDA" => {
                        match **args {
                            Atom::Nil => Err(EvalError::InvalidArgs),
                            Atom::Pair(_, box Atom::Nil) => Err(EvalError::InvalidArgs),
                            Atom::Pair(ref args, ref body) => {
                                // FIXME(DarinM223): Try to avoid cloning the environment.
                                Atom::closure(env.clone(), *args.clone(), *body.clone())
                            }
                            _ => Err(EvalError::InvalidSyntax),
                        }
                    }
                    Atom::Pair(box Atom::Symbol(ref s), ref args) if s.as_str() == "IF" => {
                        match **args {
                            Atom::Pair(
                                ref expr,
                                box Atom::Pair(
                                    ref on_true,
                                    box Atom::Pair(ref on_false, box Atom::Nil),
                                ),
                            ) => {
                                let cond = expr.eval(env)?;
                                let val = if cond == Atom::Nil { on_false } else { on_true };

                                val.eval(env)
                            }
                            _ => Err(EvalError::InvalidArgs),
                        }
                    }
                    Atom::Pair(ref op, ref args) => {
                        let op = op.eval(env)?;
                        let mut args = args.clone();
                        let mut p = &mut args as *mut Box<Atom>;

                        // FIXME(DarinM223): Instead of cloning args and modifying it in place, it
                        // might be easier and safer to just build a new list from scratch.
                        unsafe {
                            while let Atom::Pair(ref mut car, ref mut cdr) = **p {
                                **car = car.eval(env)?;
                                p = cdr as *mut Box<Atom>;
                            }
                        }

                        op.apply(*args)
                    }
                    _ => Err(EvalError::InvalidSyntax),
                }
            }
            ref p => Ok(p.clone()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use builtin::*;
    use lexer::*;
    use parser::*;

    #[test]
    fn test_interpreter() {
        let mut env = Atom::new_env(Atom::Nil);
        let mut tokens = lex("foo");

        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(
            expr.eval(&mut env),
            Err(EvalError::NoBinding(Atom::sym("FOO")))
        );

        tokens = lex("(quote foo)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("FOO")));

        tokens = lex("(define foo 42)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("FOO")));

        tokens = lex("(define foo (quote bar))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("FOO")));

        tokens = lex("foo");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("BAR")));
    }

    #[test]
    fn test_builtin() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(
            Atom::sym("CAR"),
            Atom::builtin("CAR", Box::new(builtin_car)),
        );
        env.set(
            Atom::sym("CDR"),
            Atom::builtin("CDR", Box::new(builtin_cdr)),
        );
        env.set(
            Atom::sym("CONS"),
            Atom::builtin("CONS", Box::new(builtin_cons)),
        );

        let mut tokens = lex("(define foo 1)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("FOO")));

        tokens = lex("(define bar 2)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("BAR")));

        tokens = lex("(cons foo bar)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(
            expr.eval(&mut env),
            Ok(Atom::cons(Atom::int(1), Atom::int(2)))
        );

        tokens = lex("(define baz (quote (a b c)))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("BAZ")));

        tokens = lex("(car baz)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("A")));

        tokens = lex("(cdr baz)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(
            expr.eval(&mut env),
            Ok(Atom::cons(
                Atom::sym("B"),
                Atom::cons(Atom::sym("C"), Atom::Nil)
            ))
        );
    }

    #[test]
    fn test_builtin_op() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("+"), Atom::builtin("add", Box::new(builtin_add)));
        env.set(Atom::sym("-"), Atom::builtin("sub", Box::new(builtin_sub)));
        env.set(Atom::sym("*"), Atom::builtin("mul", Box::new(builtin_mul)));
        env.set(Atom::sym("/"), Atom::builtin("div", Box::new(builtin_div)));

        let mut tokens = lex("(+ 1 1)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(2)));

        tokens = lex("(define x (* 6 9))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("X")));

        tokens = lex("x");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(54)));

        tokens = lex("(- x 12)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(42)));
    }

    #[test]
    fn test_lambda_define() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("*"), Atom::builtin("mul", Box::new(builtin_mul)));
        let mut tokens = lex("(define square (lambda (x) (* x x)))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("SQUARE")));

        tokens = lex("(square 3)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(9)));

        tokens = lex("(square 4)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(16)));
    }

    #[test]
    fn test_lambda_anon() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("-"), Atom::builtin("sub", Box::new(builtin_sub)));
        let mut tokens = lex("((lambda (x) (- x 2)) 7)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(5)));
    }

    #[test]
    fn test_lambda_curry() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("+"), Atom::builtin("add", Box::new(builtin_add)));
        let mut tokens = lex("(define make-adder (lambda (x) (lambda (y) (+ x y))))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("MAKE-ADDER")));

        tokens = lex("(define add-two (make-adder 2))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("ADD-TWO")));

        tokens = lex("(add-two 5)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(7)));
    }

    #[test]
    fn test_if_else() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("T"), Atom::sym("T"));
        let mut tokens = lex("(if t 3 4)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(3)));

        tokens = lex("(if nil 3 4)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(4)));

        tokens = lex("(if 0 t nil)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("T")));
    }

    #[test]
    fn test_predicate() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("T"), Atom::sym("T"));
        env.set(Atom::sym("="), Atom::builtin("eq", Box::new(builtin_numeq)));
        env.set(
            Atom::sym("<"),
            Atom::builtin("less", Box::new(builtin_less)),
        );

        let mut tokens = lex("(= 3 3)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("T")));

        tokens = lex("(< 11 4)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::Nil));
    }

    #[test]
    fn test_factorial() {
        let mut env = Atom::new_env(Atom::Nil);
        env.set(Atom::sym("T"), Atom::sym("T"));
        env.set(Atom::sym("="), Atom::builtin("eq", Box::new(builtin_numeq)));
        env.set(Atom::sym("-"), Atom::builtin("sub", Box::new(builtin_sub)));
        env.set(Atom::sym("*"), Atom::builtin("mul", Box::new(builtin_mul)));

        let mut tokens = lex("define fact (lambda (x) (if (= x 0) 1 (* x (fact (- x 1))))))");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::sym("FACT")));

        tokens = lex("(fact 10)");
        let expr = read_expr(&mut tokens).unwrap();
        assert_eq!(expr.eval(&mut env), Ok(Atom::int(3628800)));
    }
}
