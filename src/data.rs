use interpreter;
use std::borrow::Cow;
use std::fmt;
use std::io;
use std::rc::Rc;

pub struct Builtin {
    pub fun: Rc<Box<Fn(Atom) -> interpreter::Result<Atom>>>,
    name: String,
}

impl PartialEq for Builtin {
    fn eq(&self, other: &Builtin) -> bool {
        Rc::ptr_eq(&self.fun, &other.fun)
    }
}

impl Clone for Builtin {
    fn clone(&self) -> Builtin {
        Builtin {
            name: self.name.clone(),
            fun: Rc::clone(&self.fun),
        }
    }
}

impl fmt::Debug for Builtin {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        self.name.fmt(f)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Atom {
    Nil,
    Integer(i32),
    Symbol(String),
    Pair(Box<Atom>, Box<Atom>),
    Builtin(Builtin),
    Closure(Box<Atom>),
}

impl Atom {
    pub fn cons(car: Atom, cdr: Atom) -> Atom {
        Atom::Pair(Box::new(car), Box::new(cdr))
    }

    pub fn int(i: i32) -> Atom {
        Atom::Integer(i)
    }

    pub fn sym<'a, S>(s: S) -> Atom
    where
        S: Into<Cow<'a, str>>,
    {
        let s = s.into().into_owned();
        Atom::Symbol(s)
    }

    pub fn builtin<'a, S>(name: S, fun: Box<Fn(Atom) -> interpreter::Result<Atom>>) -> Atom
    where
        S: Into<Cow<'a, str>>,
    {
        let name = name.into().into_owned();
        Atom::Builtin(Builtin {
            name: name,
            fun: Rc::new(fun),
        })
    }

    pub fn is_list(&self) -> bool {
        match *self {
            Atom::Nil => true,
            Atom::Pair(_, ref cdr) => cdr.is_list(),
            _ => false,
        }
    }

    pub fn closure(env: Atom, args: Atom, body: Atom) -> interpreter::Result<Atom> {
        if !args.is_list() || !body.is_list() {
            return Err(interpreter::EvalError::InvalidSyntax);
        }

        {
            let mut p = &args;
            while let Atom::Pair(ref car, ref cdr) = *p {
                if let Atom::Symbol(..) = **car {
                    p = cdr;
                } else {
                    return Err(interpreter::EvalError::DifferentType);
                }
            }
        }

        Ok(Atom::Closure(Box::new(Atom::cons(
            env,
            Atom::cons(args, body),
        ))))
    }

    pub fn print<W>(&self, writer: &mut W) -> io::Result<()>
    where
        W: io::Write,
    {
        match *self {
            Atom::Nil => write!(writer, "NIL")?,
            Atom::Integer(i) => write!(writer, "{}", i)?,
            Atom::Symbol(ref s) => write!(writer, "{}", s)?,
            Atom::Pair(ref car, ref cdr) => {
                write!(writer, "(")?;
                car.print(writer)?;

                let mut atom = cdr;
                loop {
                    match **atom {
                        Atom::Nil => break,
                        Atom::Pair(ref car, ref cdr) => {
                            write!(writer, " ")?;
                            car.print(writer)?;
                            atom = cdr;
                        }
                        _ => {
                            write!(writer, " . ")?;
                            atom.print(writer)?;
                            break;
                        }
                    }
                }

                write!(writer, ")")?;
            }
            Atom::Builtin(ref b) => write!(writer, "#<BUILTIN:{:?}>", b)?,
            Atom::Closure(ref c) => c.print(writer)?,
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_print_int() {
        let expr = Atom::int(42);

        let mut output = Vec::new();
        expr.print(&mut output).unwrap();

        let output = String::from_utf8(output).unwrap();
        assert_eq!(output, "42");
    }

    #[test]
    fn test_print_symbol() {
        let expr = Atom::sym("FOO");

        let mut output = Vec::new();
        expr.print(&mut output).unwrap();

        let output = String::from_utf8(output).unwrap();
        assert_eq!(output, "FOO");
    }

    #[test]
    fn test_print_cons1() {
        let expr = Atom::cons(Atom::sym("X"), Atom::sym("Y"));

        let mut output = Vec::new();
        expr.print(&mut output).unwrap();

        let output = String::from_utf8(output).unwrap();
        assert_eq!(output, "(X . Y)");
    }

    #[test]
    fn test_print_cons2() {
        let expr = Atom::cons(
            Atom::int(1),
            Atom::cons(Atom::int(2), Atom::cons(Atom::int(3), Atom::Nil)),
        );

        let mut output = Vec::new();
        expr.print(&mut output).unwrap();

        let output = String::from_utf8(output).unwrap();
        assert_eq!(output, "(1 2 3)");
    }
}
