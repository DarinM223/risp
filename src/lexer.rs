use std::borrow::Cow;

pub fn lex<'a, S>(s: S) -> impl Iterator<Item = String>
where
    S: Into<Cow<'a, str>>,
{
    s.into()
        .into_owned()
        .replace("(", " ( ")
        .replace(")", " ) ")
        .split(" ")
        .map(|s| s.to_string())
        .filter(|s| s.len() > 0)
        .collect::<Vec<String>>()
        .into_iter()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_lex() {
        let s = "(foo bar)".to_string();
        let results: Vec<String> = lex(s).collect();
        assert_eq!(
            results,
            vec![
                "(".to_string(),
                "foo".to_string(),
                "bar".to_string(),
                ")".to_string(),
            ]
        );
    }
}
